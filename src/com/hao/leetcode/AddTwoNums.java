package com.hao.leetcode;

import com.hao.datastructure.binarytree.ListNode;

/**
 * Created by hao on 17-3-31.
 */
public class AddTwoNums {
  public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
    ListNode first = new ListNode(l1.val + l2.val);
//    ListNode first = null;
    while (l1.next != null || l2.next != null){
      ListNode node = first.next =new ListNode(l1.next.val + l2.next.val);
      first = node;
      l1 = l1.next;
      l2 = l2.next;
    }
      return first;
  }

  public static void main(String[] args) {
    ListNode l1First = new ListNode(2);
    ListNode l1Second = l1First.next = new ListNode(4);
    ListNode l1Third = l1Second.next = new ListNode(6);

    ListNode l2First = new ListNode(1);
    ListNode l2Second = l2First.next = new ListNode(2);
    ListNode l2Third = l2Second.next = new ListNode(3);
////    System.out.println(first);

    ListNode first = addTwoNumbers(l1First, l2First);
    while (first != null){
      System.out.println(first.val);
      first = first.next;
    }


  }
}
