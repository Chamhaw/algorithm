package com.hao.leetcode;

import java.util.Arrays;

/**
 * Created by hao on 3/21/17.
 */
public class Atoi {
    public static int myAtoi(String str) {
        if (null == str || "".equals(str))return 0;
        char[] charArray = str.trim().toCharArray();
        boolean negative = charArray[0] == '-';
        int position = (charArray[0] == '+' || negative) ? 1 : 0;
        int result = 0;
        while (position < charArray.length){
            if (charArray[position] < 48 || charArray[position] > 57){
                return negative? -result:result;
            }
            if (charArray.length == 1){
                return charArray[0] - '0';
            }
            result = result * 10 + (charArray[position]-'0');
            position++;
        }
        return negative ? -result : result;
    }

    public static int parseInt(String s, int radix)
            throws NumberFormatException
    {
        /*
         * WARNING: This method may be invoked early during VM initialization
         * before IntegerCache is initialized. Care must be taken to not use
         * the valueOf method.
         */

        if (s == null) {
            throw new NumberFormatException("null");
        }

//        if (radix < Character.MIN_RADIX) {
//            throw new NumberFormatException("radix " + radix +
//                    " less than Character.MIN_RADIX");
//        }
//
//        if (radix > Character.MAX_RADIX) {
//            throw new NumberFormatException("radix " + radix +
//                    " greater than Character.MAX_RADIX");
//        }

        int result = 0;
        boolean negative = false;
        int i = 0, len = s.length();
        int limit = -Integer.MAX_VALUE;
        int multmin;
        int digit;

        if (len > 0) {
            char firstChar = s.charAt(0);
            if (firstChar < '0') { // Possible leading "+" or "-"
                if (firstChar == '-') {
                    negative = true;
                    limit = Integer.MIN_VALUE;
                } else if (firstChar != '+')
                    return 0;

                if (len == 1) // Cannot have lone "+" or "-"
                    return 0;
                i++;
            }
            multmin = limit / radix;
            while (i < len) {
                // Accumulating negatively avoids surprises near MAX_VALUE
                digit = Character.digit(s.charAt(i++),radix);
                if (digit < 0) {
                    return 0;
                }
                if (result < multmin) {
                    return 0;
                }
                result *= radix;
                if (result < limit + digit) {
                    return 0;
                }
                result -= digit;
            }
        } else {
            return 0;
        }
        return negative ? result : -result;
    }

    public static void main(String[] args) {
        System.out.println(myAtoi("2147483648"));
        // "  -0012a42"
        System.out.println(Integer.parseInt(""));
        System.out.println(-Math.pow(2, 31));
        parseInt("", 10);
    }
}
