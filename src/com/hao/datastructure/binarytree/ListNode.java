package com.hao.datastructure.binarytree;

/**
 * Created by hao on 17-3-31.
 */
public class ListNode {
     public int val;
     public ListNode next;
     public ListNode(int x) { val = x; }
}
