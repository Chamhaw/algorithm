package com.hao.datastructure.binarytree;

/**
 * Created by hao on 17-3-23.
 */
public class TreeNode {
    public int value;
    public TreeNode left;
    public TreeNode right;

    public TreeNode(int value) {
        this.value = value;
    }


}
